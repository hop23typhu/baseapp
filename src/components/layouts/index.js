//layouts
import Header from './Header';
import Footer from './Footer';
import MainPage from './MainPage';
import AuthPage from './AuthPage';
import AdminPage from './AdminPage';

export { Header, Footer, MainPage, AuthPage, AdminPage };