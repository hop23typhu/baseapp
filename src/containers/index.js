import Home from './Home';
import About from './About';
import Blog from './Blog';
import Contact from './Contact';
import Services from './Services';
import Article from './Article';
import { Login, Register } from './Auth';
import { 
    Home as AdHome, 
    Article as AdArticle,
} from './Admin';
import NotFound from './NotFound';


export { 
    Home, About, Contact, Blog, Services, Article, Login, Register, NotFound,
    AdHome, AdArticle
 };